import {Chance} from 'chance';
const chance = new Chance();

class itemsGenerator {
  constructor (count) {
    const items = [];
    for (let i=0; i<count; i++) {
      items.push({
        name: chance.name(),
        count: chance.integer({min: 1, max: 6}),
        cost: chance.integer({min: 500, max: 1200})
      });
    }
    return items;
  }
}

export default itemsGenerator
