export default  {
  name: 'resizable-column',
  components: {},
  props: ['text'],
  data () {
    return {
      startX:0, startY:0, startWidth:0
    }
  },
  computed: {

  },
  mounted () {
    this.$refs.resizer.addEventListener('mousedown', this.initDrag, false);
  },
  methods: {
    initDrag(e) {
      this.startX = e.clientX;
      this.startY = e.clientY;
      this.startWidth = parseInt(document.defaultView.getComputedStyle(this.$refs.resizableDiv).width, 10);
      document.documentElement.addEventListener('mousemove', this.doDrag, false);
      document.documentElement.addEventListener('mouseup', this.stopDrag, false);
    },
    doDrag(e) {
      this.$refs.resizableDiv.style.width = (this.startWidth + e.clientX - this.startX) + 'px';
    },
    stopDrag(e) {
      document.documentElement.removeEventListener('mousemove', this.doDrag, false);
      document.documentElement.removeEventListener('mouseup', this.stopDrag, false);
    }
  }
}
